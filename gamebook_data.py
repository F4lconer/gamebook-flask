popelavy_kral_data = (
    {
        "id": 0,
        "paragraph": "Vítej, milý čtenáři!\n\nMožná už jsi o fenoménu gamebooků slyšel, možná nikoliv.\nNyní jeden takový gamebook držíš v ruce - knihu, ve které ty sám jsi hlavním hrdinou a budeš se jejím\npříběhem pohybovat na základě svých vlastních rozhodnutí.\nDovol mi ti vše vysvětlit.\nNejdřív si pozorně pročti následující stránky, vysvětlí ti základní pravidla této hry.\nAž bude vše jasné, dostaneš se na odkaz číslo 1. Od té chvíle je vše jen a jen na tobě - dle možností otáčej\nna odkazy dle svého nejlepšího uvážení a odhaluj příběh, který ti snad vyjeví svá tajemství.\nJeště něco - budeš potřebovat tužku a dvě kostky, na zápis do své Listiny Šerých Mlh.\nVše je jasné?\nTak tedy ještě jednou … Vítej!\nA nyní - pár pravidel.\nPRAVIDLA:\nNa následující straně najdeš Listinu Šerých Mlh, do které si budeš dělat poznámky o svém konání.\nAle pozor!\nJediné, co na začátku vyplň, je hodnota Kondice. Ta je rovná 12 a nikdy nemůže překročit tuto hranici, pokud\nnení v textu výslovně uvedeno jinak.\nPokud tvá Kondice někdy klesne na 0 nebo níže, okamžitě umíráš a své dobrodružství musíš začít znovu.\nNa své cestě se zcela určitě potkáš se zraněními a újmou. Kdykoliv v textu uvidíš značku (-1K), nebo\nnapříklad (-2K), sniž svou současnou hodnotu Kondice o 1 nebo 2, případně o víc, pokud by číslo před K bylo\nvyšší.\nAčkoliv naděje je v tomto příběhu pramálo, pokud bys někdy v textu narazil na symbol (+1K) a podobně, svou\nhodnotu kondice naopak o příslušnou hodnotu zvyš.\nUž jsme zmínili, že v Listině Šerých Mlh na začátku zapiš jen hodnotu počáteční Kondice.\nJakékoliv jiné údaje (např. jméno a podobně) nech prozatím prázdná. Snad budeš mít dost štěstí, aby sis\nznovu vzpomněl.\nJe také možné, že při své cestě nalezneš nějaké předměty - zapiš si je do příslušné kolonky své Listiny.\nNemůžeš mít ale nikdy více než 4 předměty. Pokud bys chtěl vzít nějaký další předmět, musíš jiný odhodit - a\ntedy vyškrtnout ze své listiny.\nDost už bylo řečí!\nAť tě štěstěna provází na tvé cestě.\nA nyní…\nTvůj příběh začíná.Jak to vlastně…začalo?\n\nJak snadno se lidská mysl upne k bláznovství, když jí hrozí zánik.\nJak snadno člověk uvěří… čemukoliv.\nPokud mu s touto pomílenou vírou svitne alespoň malý plamínek naděje.\nNaděje.\nPoslední bezpečný pokoj.\nPoslední dveře, za kterými se lze ještě schovat, zatímco odevšad útočí bolest, spalující a strašná.\nBolest, která šeptá o zničení a neodvratném konci.\nKdo by mohl přežít takové rány?\nRytíři z Alby!\nAno!\nAle kdo jsou vlastně rytíři z Alb-\nAno!\nAlba!\nV krvavé mlze plné bolesti se k tomu jménu upínáš silou tonoucího, který se chytá hozeného lana.\nA s tvým odhodláním přichází i vše ostatní.\nTvá osoba.\nTy - Rytíř z Alby. Ano, bylo to takhle… určitě! Jsi válečník, který spolu se svými bratry vytáhl do boje proti Popelavému králi, když jeho hordy nemrtvých vtrhly na území Dobré země. Vaše šiky byly jako záblesk světla proti postupujícím řadám rezavých mečů a kopí.\nKdyž se vaše šiky srazily, blýštivými meči jste prosekávali řady nemrtvých.\nPro čest a slávu… Nebo snad pro vlastní záchranu?\nTa myšlenka tě zasáhne stejnou silou, jako ostří sekery, kterou třímá kostlivá ruka.\nS výkřikem trestáš svého protivníka smrtící ranou, která navždy uhasí ledově modré plameny v prázdných očních důlcích kostlivce.\nJeho lebka praskne… Ale i tobě kvapem ubývají síly, jak ti z hluboké rány na boku uniká drahocenná krev!\n“Alba!” vykřikneš, ale místo odvahy cítíš jen obrovskou únavu.\nBojuješ dál.\nProti bodajícím nepřátelům, proti temnotě, která jak slizký had zastírá tvé smysly.\n“Alba!”\nPak přichází další rána, která roztříští tvůj štít a téměř zlomí levou ruku.\nNevnímáš, jak se z ukázněných šiků rytířů z Alby stává jen pár osamocených skupin, zoufale bojujících proti nekonečným šikům Popelavého krále, který sám rozsévá zkázu kdesi na bojišti.\nNevnímáš, kdy pod údery mocného nemrtvého v rudém tabardu padne rytíř Leantes, který ti od začátku boje oddaně stojí po boku.\nNevnímáš ránu, která ti shodí z hlavy přilbici a srazí tě do prachu.\nSvět pak milostivě přestane existovat.\n\nJak to bylo…\n\nJak zvláštní, kolik toho mysl zapomene, když je vystavena tlaku.\nJak zvláštní, kolik toho ztrácíme s každým ránem, s každou uplynulou vteřinou - vzpomínky na maličkosti, na jména dávno zemřelých, na barvu očí našich milovaných.\nPřesto jsou však události, lidé a myšlenky, které z mysli nemizí.\nIdeály, kterým jsme zasvětili celý svůj život.\nSliby.\nPřísahy, pro které se postavíme zvůli osudu i hrůze z nočních můr, co má příchuť strachu a jedu odkapávajícím z dlouhých nožů a dýk.\nAlba.\nIdeál rytířství, kterému patří nyní tvůj život i tvá duše.\nIdeál, který nemohl nečinně přihlížet, když vojska démonického Popelavého krále vtrhla na území Dobré země. Přišlo to tak náhle. Jako lavina se démonický král i jeho nekonečné šiky neživých Legionářů přelily přes hranice Dobré země a svými zrezavělými meči a pařáty rozsévaly strach.\nJakoby před tímto sprostým vpádem nebylo nic jiného.\nA taky Rytíři z Alby vyrazili.\nJejich bílé prapory byly jako bouře světla, které šlehalo proti hrozivé noci, kterou s sebou nesla smrtící magie Popelavého krále.\nAle jakou moc má světlo proti všepohlcující temnotě?\nTakovým myšlenkám bylo prosté se bránit, když jsi stál bok po boku svým druhům z Alby.\nKdyž se na pláních vaše vojska střetla, tvůj dlouholetý výcvik tě vedl. Zrezlé meče se tříštily o tvé blýštivé ostří a ty jsi odpovídal přesnými protiútoky a měnil zplozence samotných pekel v prach.\nCíl byl jasný - Popelavý král musí padnout, aby se jeho zlo navždy vytratilo z tohoto světa.\nAvšak…\nDémonický král, dle mnohých zplozenec samotné Smrti, vládl nesmírnou mocí.\nA svou moc propůjčoval svým vojskům plnou silou.\nJak zvláštní, kolik toho mysl zapomene, když je vystavena tlaku.\nNepamatuješ si, kdy se začala hroutit křídla vaší armády.\nNepamatuješ si, kdy jsi utržil první ránu a kdy tu poslední.\nNepamatuješ si, kdy padl rytíř Leantes, který ti do posledního dechu stál po boku.\nNepamatuješ si, kdy se z hrdé armády Alby stal osamocený ostrůvek zoufalců.\nKdo z tvých druhů jako první odhodil v panice zbraň a dal se na útěk.\nKdo jako první uviděl tělo gigantického Popelavého krále a vykřikl varování?\nKdo zemřel jako první, když z úst démonického pána vylétly stravující plameny?\nNepamatuješ si.\nDo boku tě zasáhne ostrá čepel a bolest je téměř nesnesitelná.\nPoslední, co vidíš, je stěna plamenů Popelavého krále, která pohlcuje vše, co mu stojí v cestě.\nPak tě obklopí temnota.\n\n\n",
        "decisions": {
            "1": "START",
        },
        "events": {"life": -1},
    },
    {
        "id": 1,
        "paragraph": "Probudí tě ostrá bolest.\nPrudce otvíráš oči a pudově kolem sebe biješ rukama, abys zabránil nemrtvým, aby si jejich shnilé zuby pochutnaly na tvém mase.\nOdpovědí je ti jen rozhněvané krákání a mávání křídel.\nZ mělké rány na čele ti do očí steče trochu krve.\nTen zatracený pták by ti snad vykloval oči!\nSnažíš se rozhlédnout se kolem sebe, zatímco ti v hlavě víří myšlenky jako uragán…\nTen boj.\nPopelavý král.\nTví druzi…\nAlba!\nPři té vzpomínce prudce vstaneš - a jen s vypětím všech sil potlačíš výkřik bolesti.\nZatímco kdesi nad tebou stále zní krákání, nedokážeš se nepodívat na ránu, kterou ti způsobila rezavá sekera.\nRána je hluboká, svou zbroj i rytířský šat máš zmáčený krví.\nDoslova cítíš, jak ti mezi prsty uniká vlastní život.\nZnamená to snad…\nZemřeš.\nS tím poznáním přichází zvláštní klid… A také poznání, že si nevzpomínáš ani na své jméno, natož na jména ostatních, nebo podrobnosti o svém životě před tímto bojem.\nKdo vlastně jsi?\nRozhlížíš se po ztichlém bojišti, zahaleném nocí, která provází běsnění Popelavého krále a mezi zášlehy bolesti si uvědomuješ, co musíš udělat.\nNedokážeš zachránit nevinné, kteří žijí v blízkých vesnicích a městech.\nDokážeš pouze zemřít na tomto bojišti.\nOtázka však zůstává…\nCo dokážeš, než se tak stane.\nJak se rozhodneš, rytíři z Alby?",
        "decisions": {
            "2": "Můžeš se pokusit vydat se vpřed temnou nocí",
            "3": "Nebo zde můžeš jednoduše počkat… až vše skončí",
        },
    },
    {
        "id": 2,
        "paragraph": "(-1K)\nMeč v tvé ruce, kdysi hrdý odznak tvého řádu, je nyní jen těžkým břemenem.\nPřesto… Býval tvou součástí.\nStejně jako… ideály. Ano. Pomáhat potřebným…\nPři tom uvědomění jakoby v tobě kousek ožil.\nPomáhat potřebným.\nTo je… část tvého já.\nCítíš, že abys poskládal celou mozaiku sama sebe, budeš jich potřebovat mnohem víc.\nA jestli se ti jí podaří složit… Budeš mít odvahu podívat se do tohoto zrcadla?\nTiskneš jílec zbraně v dlani.\nSnad ti dopomůže na tvé cestě složit tvůj vlastní obraz.\nS vypětím všech sil vstáváš z bojiště, které tě již přijalo mezi mrtvé.\nKolem tebe není nic než tma, jen tu a tam z temnoty vystoupí nejasný obrys.\nKam se vydáš?",
        "decisions": {
            "5": "Pokud se chceš vydat tmou nazdařbůh vpřed",
            "4": "Pokud chceš pátrat po tom, co se stalo s tvým druhem Leantem",
        },
        "events": {"life": -1},
    },
    {
        "id": 3,
        "paragraph": "Posadíš se na místo, ze kterého jsi jen před chvílí vstal, zatímco ti krev vytéká z ran.\nOkolní noc tě obestírá a před očima ti tančí vzpomínky na minulost…\nAle jsou zahaleny stínem a ty nemáš sílu pokračovat dál… Možná máš strach z toho, co bys odhalil.\nKdyž tě najde Popelavý král, cítíš v jeho pohledu zvláštní smutek.\nPak ale smutek ustupuje a svým mocným mečem zasáhne tvé srdce dokonalou, smrtící ranou.\nKonec je milostivě rychlý.\nTvůj příběh zde končí a jeho tajemství… zůstává skryto.\n\n",
        "decisions": {},
        "events": {"death": 1},
    },
    {
        "id": 4,
        "paragraph": "První krok je těžký, jako bys hýbal celým světem.\nDalší… není o moc lepší.\nPřesto nepřestáváš.\nKdesi nad sebou zaslechneš havraní krákání, ale přijde ti to spíš jako smích.\nVítězoslavný? Posměšný? Těžko říct.\nPostupuješ krok po kroku temnotou. Ta svá tajemství vydává nelibě, jako žíznivec, co se má podělit o poslední hlt vody.\nPřekračuješ své padlé druhy a jejich puklé zbroje, i zbytky nemrtvých.\nVoláš Leantovo jméno, ale neodpovídá.\nA neodpoví už nikdy.\nNalezneš ho opřeného zády o padlý strom, s tváří otočenou k zemi.\nZ hrudi mu trčí násada kopí a jeho krev se tiše vpíjí do okolní země.\nBolest z jeho ztráty tě zasáhne jako další utržená rána - jen tato nekrvácí.\nVšimneš si, že u pasu má tvůj padlý druh krátkou, blýštivou dýku…\nŽivot mu ale nezachránila.\n",
        "decisions": {
            "6": "Pokud se mu chceš podívat do tváře, abys ses s ním mohl rozloučit",
            "7": "Pokud si od něj chceš vzít jeho dýku",
            "5": "Pokud se za něj chceš jen tiše pomodlit a vydat se dál",
            "3": "Nebo je toho na tebe možná příliš - a ty si chceš prostě jen sednout a počkat na konec…",
        },
    },
    {
        "id": 5,
        "paragraph": "Kráčíš dál - a srdce se ti svírá žalem, když ti okolní temnota vyjevuje, co bylo dříve skryto.\nHlavou ti bleskne, že tma Popelavého krále je vlastně milosrdná.\nA přestože to zní jako rouhání, ta myšlenka se ti usadí na duší.\nKráčíš po bojišti, které je doslova poseté mrtvými.\nBílé pláště pánů z Alby jsou rudé krví, kdysi hrdé prapory jen zplihle leží na rozbahněné zemi.\nMáš pocit, že kdesi z dálky zaznívá umíráček.\nAle pro koho zvoní?",
        "decisions": {
            "8": "Pokud se chceš na bojišti porozhlédnout po něčem užitečném",
            "9": "Pokud se chceš vydat dál",
        },
    },
    {
        "id": 6,
        "paragraph": "Skloníš se k padlému druhovi a se smutkem mu pravačkou zvedneš hlavu, aby sis jej ještě znovu prohlédl -\nA se zděšením ucukneš, když zjistíš, že hledíš do zjizvené tváře a do prázdných, rozdrápaných očních důlků.\nCítíš, jak se ti roztřesou ruce.\nJen s vypětím všech sil potlačuješ dávení a utíkáš tmou pryč, ačkoliv dobře víš, že tenhle pohled nikdy nezapomeneš.",
        "decisions": {"5": "Pokračuj"},
    },
    {
        "id": 7,
        "paragraph": "Dýka jde z kožené pochvy překvapivě snadno.\nI přes okolní tmu se blýská, jako by snad zářila vlastním světlem. Zasuneš si ji za opasek - třeba ti poslouží lépe, než svému předchozímu majiteli.",
        "decisions": {
            "5": "(Zapiš si Leantovu dýku do své Listiny mezi Předměty)\nVydáš se na cestu temnotou dál",
            "6": "Podíváš se Leantovi do tváře, abys ses s ním mohl rozloučil",
            "3": "Nebo je toho na tebe možná příliš - a ty si chceš prostě jen sednout a počkat na konec… ",
        },
    },
    {
        "id": 8,
        "paragraph": "Mezi zpřelámanými kostmi a zlomenými meči najdeš jen málo, co by ti mohlo pomoci.\nVojska démonického krále již zřejmě vše cenné odnesla, aby to obětovala svému pánu na temných oltářích.\nPřesto se na tebe nakonec usměje štěstí.\nV sedlové brašně jednohoho z mrtvých koní nacházíš čutoru s vodou a pár sušených bobulí.\nAniž bys přemýšlel, v druhé chvíli už hltavě piješ - na ránu na boku pro tu chvíli vůbec nedbáš.\nNa hlad nemáš ani pomyšlení - pokud ale chceš, můžeš si sušené plody vzít s sebou (pak si zapiš sušené bobule do své Listiny jako předmět).\nKdesi v dálce znovu zazní zvon umíráčku….",
        "decisions": {
            "9": "Pokračuj dále abys poznal, kam tě ten pochmurný zvuk dovede"
        },
    },
    {
        "id": 9,
        "paragraph": "(-1K)\nV boku ti vybuchne ostrá bolest, jak se tvá rána znovu připomene.\nBylo by tak špatné se jí poddat?\nTa myšlenka je vlastně až svůdná…\nPak skrze mdloby, které tě hrozí pohltit, uslyšíš známý, nepříjemný zvuk.\nKdesi nad tebou kráká havran.\nUcítíš na tváři závan vzduchu - a v další chvíli hledíš do černých korálkových očí drobného ptáka, který se uhnízdil naproti tobě na jílci rezavého meče, který ční ze zad jednoho z padlých rytířů.",
        "decisions": {
            "10": "Pokud si chceš opeřence nejdřív jen prohlížet, otoč na",
            "12": "Pokud jej chceš odehnat, otoč na\nPokud máš nějaké jídlo a chceš mu ho nabídnout, pokračuj na",
            "11": "Pokud na něj chceš promluvit, otoč na",
            "14": "Pokud žádné jídlo nemáš (nebo mu ho nechceš nabídnout), můžeš jednoduše vyrazit dál - v tom případě pokračuj na",
        },
        "events": {"life": -1},
    },
    {
        "id": 10,
        "paragraph": "Na první pohled havran vypadá úplně obyčejně - má černé peří, zobák i oči, kterými tě pozoruje.\nKdyž si jej prohlížíš blíž, všimneš si, že ani jemu se hněv Popelavého krále nevyhnul - pírka má tu a tam vytrhaná či ohořelá. Zvířecí očí jsou unavené a zarudlé.\nMáš neurčitý pocit, že stejně, jako si ty prohlížíš jeho, studuje i on tebe.",
        "decisions": {
            "13": "Pokud jej chceš odehnat, otoč na",
            "12": "Pokud máš nějaké jídlo a chceš mu ho nabídnout, zkus to otočením na",
            "11": "Pokud na něj chceš promluvit, otoč na",
            "14": "Případně můžeš jednoduše vyrazit dál a havrana si nevšímat, pak otoč na",
        },
    },
    {
        "id": 11,
        "paragraph": "Mluvíš na havrana lidskou řečí a vzpomínáš na dávné legendy, které jsi slýchával.\nV myslí se ti při tom mihne vzpomínka na ženu ve středních letech, se tmavými vlasy a s úsměvem na tváři… Její jméno ti ale uniká.\nByla snad tvou matkou?\nUvědomíš si, že zatímco si byl ztracen v myšlenkách, havran naklonil pobaveně hlavu na stranu.\nNeříká nic, jen tě dál pozoruje.",
        "decisions": {
            "13": "Pokud jej chceš odehnat, otoč na",
            "12": "Pokud máš nějaké jídlo a chceš mu ho nabídnout, pokračuj na",
            "10": "Pokud si jej chceš chvíli prohlížet, otoč na",
            "14": "Případně můžeš jednoduše vyrazit dál a havrana si nevšímat, pak pokračuj na",
        },
    },
    {
        "id": 12,
        "paragraph": "Vytáhneš z tlumoku sušené bobule a nabídneš je havranovi.\nChvíli tě podezřívavě pozoruje… Ale pak je sezobe stejně rychle, jako jsi ty před pár okamžiky vypil vodu z čutory.\nKdyž se nají, spokojeně zakráká, a znovu vzlétne do vzduchu.\nMožná, že jsi právě vykonal svůj poslední dobrý skutek - dal jsi najíst zatoulanému havranovi.\nPři té myšlence se ti na tvář vloudí stín úsměvu…\nV další chvíli se stane cosi, co ti připomene, že naděje na tomhle prokletém místě zemřela již dávno.",
        "decisions": {"15": "Pokračuj"},
    },
    {
        "id": 13,
        "paragraph": "Protivného havrana budeš na své cestě jen stěží potřebovat!\nSebereš ze země kámen a hodíš ho po ptákovi.\nTen máchne křídly a rozzlobeně zakráká - a během několika dalších chvil je pryč.\nTak - teď snad konečně budeš mít klid…\nV další chvíli se stane cosi, co ti připomene, že klidu se na tomto prokletém místě nedostává.",
        "decisions": {"15": "Pokračuj"},
    },
    {
        "id": 14,
        "paragraph": "Když vyrazíš dál, pták mávne křídly a za pár chvil se odkudsi nad tebou opět ozývá odhodlané havraní krákání.\nV další chvíli se stane cosi, co ti připomene, že naděje na tomhle prokletém místě zemřela již dávno.",
        "decisions": {"15": "Pokračuj"},
    },
    {
        "id": 15,
        "paragraph": "Ze tmy po tvé levici se ozve hrdelní vrčení.\nTma, která jinak vše skrývá, se na pár úderů srdce rozestoupí…\nA z ní přichází hrůzostrašná postava.\nJe oděná ve staré, zrezlé zbroji, v bledé tváři ji hoří rudé oči.\nSe zatajeným dechem poznáváš jednoho z nemrtvých legionářů.\nSe svými druhy z Alby jsi byl cvičen bojovat proti tomu, co představují.\nNyní jsou ale tví druzi mrtví a ty stojíš proti nemrtvému běsu sám.\nTvůj protivník ti nedává žádný čas na rozmyšlenou.\nS posměšným zasyčením pozvedne rezavé kopí a vrhne se proti tobě.\nBojuj, rytíři z Alby! Nebo tohle krví nasáklé bojiště schvátí další oběť…\nAbys vyřešil tento boj, hoď dvěma kostkami.\nPokud máš při sobě Leantovu dýku, přičti 2 k výslednému součtu.\nPokud je výsledek 0-4, otoč na 16\nPokud je výsledek 5-7, otoč na 17\nPokud je výsledek 8 nebo více, otoč na 18\n\n",
        "decisions": {
            "16": "Pokud je výsledek 0-4",
            "17": "Pokud je výsledek 5-7",
            "18": "Pokud je výsledek 8 nebo více",
        },
        "events": {"dice": "2d6"},
    },
    {
        "id": 16,
        "paragraph": "Srazíte se s nemrtvým válečníkem znovu a znovu, tiché bojiště je náhle plné zvuků boje.\nTvůj protivník jen mlčí, na tváři pokřivený škleb.\nV jednu chvíli se ti podaří srazit jeho kopí stranou - a v druhé ti zatne své drápy hluboko do stehna (-2K).\nBolest dodává sílu tvým znaveným pažím.\nS výkřikem plným hněvu mu tvrdým úderem rozpoltíš lebku.\nNemrtvý padne bez hlesu k zemi, tentokrát nadobro.\nKdyž je konečně po boji, rychle se pokusíš ošetřit si ránu.\nJako provizorní obvaz poslouží utržený kus kdysi hrdé vlajky Alby.\nNapůl čekáš, že se z temnoty kolem tebe vyřítí další nepřátelé, přilákání vaším zápolením…\nTo se ale nestane.\nTma kolem tebe jakoby tímto vítězstvím o trochu prořídla. Máš pocit, že tě zve k sobě.\nNenecháš ji čekat dlouho.",
        "decisions": {"19": "Pokračuj"},
        "events": {"life": -2},
    },
    {
        "id": 17,
        "paragraph": "Ačkoliv jsi zraněný, tvůj bojový pokřik na pár úderů srdce rozezní podivně ztichlé bitevní pole.\nNemrtvý nezaváhá a s hrdelním chrčením zaútočí. Za svého života musel být jistě obstojným šermířem, jeho kopí se míhá vzduchem v nebezpečných obrazcích a jednou ti dokonce prosviští těsně kolem obličeje.\nV příštím okamžiku mu srazíš hnijící hlavu z ramen.\nPadne bez hlesu k zemi. Magie, která jej přivedla do světa živých, jej rychle opouští.\nKdyž nebezpečí pomine, skloníš znaveně meč - a zakleješ, když si všimneš, že ti kopí legionáře způsobilo nepříjemnou ránu na pravém rameni (-1K).\nNapůl čekáš, že se z temnoty kolem tebe vyřítí další nepřátelé, přilákání vaším zápolením…\nTo se ale nestane.\nTma kolem tebe jakoby tímto vítězstvím o trochu prořídla.\nMáš pocit, že tě zve k sobě.\nNenecháš ji čekat dlouho.",
        "decisions": {"19": "Pokračuj"},
        "events": {"life": -1},
    },
    {
        "id": 18,
        "paragraph": "Nemrtvý legionář musel být za svého života obstojným šermířem, ale ačkoliv jsi zraněný a vyčerpaný, tvůj výcvik vede tvé pohyby.\nVyčkáš na protivníkův útok, odrazíš ostré kopí, které ti směřovalo na hruď a plynulým úderem setneš nemrtvému hlavu z ramen.\nV druhém okamžiku se nemrtvý zhroutí k zemi a již nevstane.\nVyšleš tichou modlitbu k nebesům za toto malé vítězství.\nRána v boku ti ale rychle připomene, že tvůj čas se i tak krátí.\nNapůl čekáš, že se z temnoty kolem tebe vyřítí další nepřátelé, přilákání vaším zápolením…\nTo se ale nestane.\nTma kolem tebe jakoby tímto vítězstvím o trochu prořídla.\nMáš pocit, že tě zve k sobě.\nNenecháš ji čekat dlouho.",
        "decisions": {"19": "Pokračuj"},
    },
    {
        "id": 19,
        "paragraph": "Kde byla předtím neprostupná čerň, vidíš nyní před sebou tři cesty.\nKde byla dříve tma, jakoby přicházelo jitro, pomyslíš si.\nJitro…\nCítíš, že to slovo je pro tebe důležité, ale nedokážeš si vzpomenout, proč.\nUpřeš pohled na cesty před sebou.\nCesta po tvé levici vede k jakémusi podivnému obrazu, který snad nějaký pomatený umělec pohodil na tomto bojišti.\nCesta vpravo končí v jakési podivné tůni, kolem které rostou pokroucené stromy.\nCesta před tebou pak vede jednoduše vpřed a do daleka.\nHlavou ti proběhne znepokojivá myšlenka:\nMožná se v temnotě skrývají odpovědi. Možná ale nejsi na věci v temnotě připravený.",
        "decisions": {
            "20": "Pokud se chceš vydat k podivnému obrazu po levici, otoč na",
            "21": "Pokud chceš prozkoumat zvláštní tůni, vydej se k ní otočením na",
            "26": "Pokud se chceš vydat vpřed a nechat tak ostatní dvě cesty za sebou, pokračuj na",
        },
    },
    {
        "id": 20,
        "paragraph": "Kráčíš cestou k matnému rámu, hlavu plnou otázek.\nVzal s sebou snad někdo z Alby některý z obrazů dávných velmistrů vašeho řádu, aby vás jejich zvěčněné podoby inspirovaly k udatným skutkům?\nJak docházíš blíž, dojde ti, že to není portrét minulých představených z Alby.\nNení to vlastně ani obraz.\nKdosi tu do hluboké rýhy zaklínil zrcadlo s bohatě vykládaným rámem.\nKdyž se snažíš najít cestu kolem, dojde ti, že dál neprojdeš.",
        "decisions": {
            "24": "Podíváš se do zrcadla",
            "25": "Rozbiješ zrcadlo jedním z okolo ležících kamenů",
            "19": "Vrátíš se na rozcestí - tuto možnost ale již zvolit nemůžeš",
        },
    },
    {
        "id": 21,
        "paragraph": "Tvé kroky tě zavedou k malé tůni, s vodou černou jako sama noc. Kolem rostou pokroucené stromy a jejich větve sténají v neexistujícím větru. Dál cesta nevede, na křivé kmeny se slizce lepí neprostupná temnota.\nJe ticho.\nKdyby to nebylo místo uprostřed bojiště, mohlo by snad být i místem rozjímání… a spočinutí.",
        "decisions": {
            "22": "Vyčkáš, jestli se něco nestane",
            "19": "Vrátíš se zpět - tuto možnost ale již zvolit nemůžeš",
        },
    },
    {
        "id": 22,
        "paragraph": "(-1K)\nUž se chceš otočit, když si všimneš, jak se na hladině cosi pohne.\nRychle se k ní skloníš a na okamžik oněmíš úžasem.\nPod hladinou vidíš své druha, rytíře Leanta.\nStojí hrdě rozkročený a pozoruje tě svýma vždy přátelskýma očima. Nemá na sobě ani škrábnutí.\nPo chvíli pak k tobě zvedne pravici. Stačilo by jen ponořit ruku do tůně, abyste se dotkli.",
        "decisions": {
            "23": "Podáš Leantovi ruku",
            "19": "Vrátíš se zpět - tuto možnost ale již zvolit nemůžeš",
        },
        "events": {"life": -1},
    },
    {
        "id": 23,
        "paragraph": "Když se tvá unavená dlaň dotkne vodní hladiny, ochromí tě nadpřirozený chlad.\nZkoušíš se napřímit, ale svaly tě neposlouchají a ty pomalu klesáš na kolena… a pak celý přepadneš do tůně.\nNa okamžik tě zachvátí panika, ucítíš krátkou bolest…\nAle pak spatříš Leantův úsměv a když tě obejme kolem ramen, víš, že ve smrti budete spolu.\nTvůj příběh zde… sladkobolně… končí.\n\n",
        "decisions": {},
        "events": {"death": 1},
    },
    {
        "id": 24,
        "paragraph": "S lehce rozechvělýma rukama přistupuješ k zrcadlu.\nNa jeho povrchu vidíš svou vlastní strhanou tvář, plnou jizev.\nNepřipadáš si v tu chvíli jako rytíř.\nPřipadá ti, že na tebe shlíží obličej prostého, obyčejného člověka.\nA není tomu tak? Vždyť přece ten rychlý výjezd se spanilou Chiarou, co měla vždy vlasy jako slunce…\n(Zapiš si jméno Chiara do kolonky jméno princezny. Vzpomněl sis na to.)\nPři té vzpomínce se zachvěješ, ale tvá mysl ti nedovolí spatřit jí celou, abys nepropadl šílenství.\nTma Popelavého krále… je vlastně milosrdná.\nNyní ti to jako rouhání nezní.\nStále rozechvělý o několik kroků poodstoupíš.",
        "decisions": {
            "25": "Rozbiješ zrcadlo jedním z okolo se povalujících kamenů",
            "19": "Vrátíš se na rozcest - vyber si jinou z cest, pamatuj však, že k zrcadlu se již vrátit nemůžeš",
        },
        "events": {"princess": "Chiara"},
    },
    {
        "id": 25,
        "paragraph": "Sebereš jeden z kamenů a hodíš ho vší silou proti lesklému povrchu zrcadla.\nTo se rozletí na nespočet kusů - jeden z nich tě sekne do levé tváře (-1K).\nTeď už svou nepříjemnou pravdou ta blýštivá tretka nikoho trýznit nebude!\nChvíli tiše pozoruješ všude se válející střepy, které odráží tvou siluetu v potrhaných záblescích…",
        "decisions": {
            "19": "Vyrazíš zpět - vyber si jinou z cest, sem se již vrátit nemůžeš"
        },
        "events": {"life": -1},
    },
    {
        "id": 26,
        "paragraph": "Jdeš cestou vpřed a brzy necháváš místo svého střetu s nemrtvým legionářem v dálce za sebou.\nKolem panuje nepřirozené příšeří a přísahal bys, že tu a tam slyšíš z okolní temntoty šepot - jakoby tě kdosi volal a lákal k sobě do tmy.\nSnažíš se nevnímat naléhání přízračných hlasů, ale únava a bolest způsobí, že při jednom zvlášť naléhavém volání se ohlédneš - a srdce ti na okamžik vynechá.\nZa hradbou šera vidíš několik hrobů. Jsou nové, ještě se na nich nepodepsal zub času - jakoby mistr kameník teprve před několika chvílemi dokončil své chmurné dílo.\nKdyž hledíš na ten zvláštní výjev, odkudsi zavane vítr - a dva z hrobů se náhle ocitnou téměř u tebe, jen sáh za hradbou tmy.\nRychle si všimneš, co je na hrobech zvláštního.\nTam, kde by měla být jména zemřelých, je prázdné místo.\nS rostoucím děsem v srdci si uvědomuješ, že ta jména znáš, ačkoliv si nemůžeš vzpomenout proč.\nJsou pro nás vlastně mrtví…\nCo je to za myšlenku?\nVe stejnou chvíli se ti v rukou jako kouzlem objeví malé kladívko a dláto.",
        "decisions": {
            "27": "Pokud chceš do kamene vytesat jména zemřelých, vykonej tak otočením na",
            "28": "Pokud to učinit nechceš, můžeš dláto i kladívko odložit a vydat se cestou vpřed - pak pokračuj na",
        },
    },
    {
        "id": 27,
        "paragraph": "Se zatajeným dechem pozvedáš kamenické náčiní a necháš instinkty i sám osud, aby tě vedly.\nVíš dobře, že s těmito jmény ses ještě od svého probuzení na tomto bojišti nesetkal… Ale přesto cítíš - víš - že jsou s tebou nerozlučně spjata.\nKdyž tesáš jednotlivá písmena do chladného kamene, cítíš, jak ti po tvářích tečou slzy.\nV mysli ti vytane několik obrazů.\nSpolečná přísaha. Pomáhat potřebným. Pak to volání o pomoc… A oni jsou příliš daleko, aby pomohli… Jsou… vlastně mrtví, příliš daleko. Nestihnou to.\nMyšlenka rychle skončí, aby tě alespoň částečně ochránila před bolestí.\nNa okamžik zavřeš oči…\nKdyž je znovu otevřeš, vidíš před sebou vytesaná dvě jména.\nA víš, že jsou to jména tvých přátel, tvých družiníků (Vymysli si dvě prostá, obyčejná jména, se kterými jsi se v tomto příběhu ještě nesetkal. Zapiš je na vyznačené řádky níže. Jsou to jména tvých přátel, ačkoliv na podrobnosti si zatím nevzpomínáš).\n\nJosef\n\nNikola\n\nS těžkým srdcem dáváš sbohem tichým hrobům a odkládáš kladívko i dláto. Když vyrážíš cestou dál, cítíš bolest… Ale také tíhu, která ti pomalu padá z ramen.",
        "decisions": {"28": "Pokračuj ve své cestě"},
    },
    {
        "id": 28,
        "paragraph": "(-1K)\nNecháváš místo posledního odpočinku mrtvých za sebou a přes pohlcující bolest z ran, které jsi utržil, postupuješ dál. Několikrát se ti podlomí nohy, ale nedbáš toho. Znovu a znovu vstáváš a pokračuješ vpřed.\nPo chvíli uslyšíš zvláštní zvuk. Nejdříve nevěříš svým uším, ale pak to pochopíš, když tvé oči spočinou na modré hladině…\nMalé vlnky jemně naráží do břehu, na který jsi právě dorazil.\nPřed tebou, kam až oko dohlédne, se rozprostírá vodní hladina.\nOpodál vidíš opuštěné molo.\nDochází ti, že jediná cesta vpřed… Vede přes modrou nádheru.\nDosáhl jsi mořského pobřeží.",
        "decisions": {
            "29": "Pokud se chceš pokusit plavat na druhou stranu, otoč na",
            "30": "Pokud chceš počkat, zda-li k molu nedorazí nějaká loď, otoč na",
        },
        "events": {"life": -1},
    },
    {
        "id": 29,
        "paragraph": "Voda je chladná a když ti dosahuje po kotníky, příjemně chladí a osvěžuje. Když ti však začíná dosahovat až k pasu, cítíš, jak tě její tíha táhne ke dnu.\nKdyž ti dojde, že na přeplávání nemáš sílu, pokusíš se doplavat ke břehu…\nAle tvé rány ti vzaly už příliš mnoho sil.\nMoře vzpomínek tě pohltí bez nářku… Ale i bez slitování.\nTvůj příběh zde tragicky končí.",
        "decisions": {},
        "events": {"death": 1},
    },
    {
        "id": 30,
        "paragraph": "Čekáš, zatímco okolo šumí moře.\nOčima zalétneš vzhůru, kde vidíš stále stejně šero, bez náznaku toho, že by se mělo kdy rozednit.\nV mysli se ti objeví vzpomínka na Albu. Ale ačkoliv se snažíš sebevíc, nedokážeš si vzpomenout na společné tréninky, na vaše panství… Vše se jen halí do temnoty a stínů, které se tě snaží oklamat. Jediné, co si uvědomíš, je, že Alba… Alba znamená rozbřesk.\nByli jste snad rytíři Rozbřesku?\nAni tím si nejsi zcela jistý.\nCítíš, že v tobě roste zoufalství.\nCo se to s tebou stalo?\nProč jsi tady?\nA pak se ti připomene otázka, které se bojíš ze všeho nejvíc.\nKdo vlastně jsi?\nRozechvěješ se po celém těle, až padneš do písku, který je nasáklý mořskou vodou.\nPak se ale stane něco neočekáváného.",
        "decisions": {"31": "Pokračuj"},
    },
    {
        "id": 31,
        "paragraph": "Skrze tvé zoufalství proniknou tóny bubnu.\nBuch-buch! Buch-buch!\nPrudce zvedneš hlavu a vidíš, jak tě míjí zvláštní procesí.\nV podivném hloučky přichází od bojiště posetého mrtvolami tvých bratří a nemrtvých zvláštní průvod. Tři ženy a dva muži jsou oděni v kejklířském oděvu, který je však ušitý z černých a šedých látek, takže vypadají spíše jako pozůstalí.\nBuch-buch! Buch-buch!\nKdyž si tě všimnou, jak ležíš v mokrém písku, jedna z žen, mladá dívka, se od hloučku oddělí a vyrazí k tobě, s prostým proutěným košíkem v ruce.\nBuch-buch! Buch-buch!\nDívka dojde až k tobě, vidíš její bosé nohy, které se boří do písku, jen nedaleko od tvé hlavy.\nKdo jsem?\nOtázka se znovu vrátí a rozechvěje tě, třeseš se jako v horečce.\nPak promluví bubeník:\n“Další mrtvý?” zeptá se a na okamžik přestane bubnovat.\nNáhle se ti nedostává dechu a cítíš neskutečnou tíhu na hrudi.\nJe tohle konec?\nDívka se nadechuje k odpovědi, kterou na tomto bojišti jistě již dala mnohokrát.\nPak ale nakloní hlavu na stranu a chvíli, kdy tě tíha na prsou drtí jako kovadlina, mlčí.\n“Nevím,” odpoví nakonec.\nSnažíš se bojovat proti tíze světa, která se na tebe valí, říct ji, že tvůj příběh ještě neskončil. Je to však jako snažit se pohnout skálou.\nHoď dvěma kostkami a přičti momentální hodnotu svého zdraví.",
        "decisions": {
            "32": "Pokud je výsledek 7 a méně",
            "33": "Pokud je výsledek 8 nebo více",
        },
    },
    {
        "id": 32,
        "paragraph": "Snažíš se zavolat na tu zvláštní dívku, že nepatříš mezi padlé, že v tobě je ještě kousek života…\nAle bubeníkovo Buch-buch se už neozve.\nA v tvém zraněném, vyčerpaném těle náhle už není tlukoucí srdce, které by jeho píseň doplnilo.\nTvůj příběh končí zde, na břehu Moře Vzpomínek.\n\n",
        "decisions": {},
        "events": {"death": 1},
    },
    {
        "id": 33,
        "paragraph": "Ačkoliv je tíha strašlivá, tlačíš proti ní celou svou vůlí, ačkoliv je to nemožné.\nPokoušíš se formulovat slova, ale z tvých rozpraskaných rtů vyjde jen pološílené zachrčení…\nA tlak, který tě drtí, se již nedá vydržet-\n“Žije!”\nPři dívčině překvapeném zvolání bubeník znovu začne hrát.\nBuch-buch! Buch-buch!\nTíha na prsou pomalu přechází.\nJakoby byl bubeníkův rytmus pro tebe vším.\nVysíleně se převalíš na záda.\nSlyšet bít své srdce je náhle neuvěřitelně krásné.\nOtoč na 34\n\n",
        "decisions": {"34": "Otoč na"},
    },
    {
        "id": 34,
        "paragraph": "Možná se ti to zdá, ale máš pocit, jakoby černá noc o trochu zesvětlala.\nDívka se k tobě skloní, ale černovlasý muž z pochmurného procesí ji zastaví:\n“Pozor! To nemůžeme!”\nPři tom napomenutí dívka strne, ale poslechne.\nKdyž se jí zadíváš do očí, jsou plné smutku:\n“Máte pravdu. Mohu se ho alespoň na něco zeptat?”\nMuž, který ji dříve napomínal, se zachmuří:\n“Dobrá. Ale jen jednu otázku. Utíká čas!”\nDívka se plaše usměje a zadívá se ti hluboko do očí.\n“Řekněte mi, na co se mám zeptat!”",
        "decisions": {
            "35": "Pokud chceš, aby se tě zeptala na to, jak jsi se sem vůbec dostal, požádej ji",
            "36": "Pokud chceš, aby se tě zeptala na tvé jméno",
            "37": "Pokud chceš, aby se tě zeptala na poslání tvého řádu",
            "38": "Pokud chceš, aby se tě raději na nic neptala a neoživovala bolestivé vzpomínky",
        },
    },
    {
        "id": 35,
        "paragraph": "Když chraplavě vyslovíš své přání, dívka pomalu kývne hlavou:\n“Jak jsi se sem dostal, člověče?”\nPřekvapí tě, že tě neoslovuje jako rytíře, ale cítíš, že je to tak… správně?\nV další chvíli tě pohltí záblesky vzpomínek.\nVidíš sám sebe, jak se směješ a říkáš, že se není čeho bát. Vidíš před sebou raněného, ke kterému se rychle vrháš, jistý si sám sebou. Odkudsi za sebou pak slyšíš vyrovný výkřik a jakési burácení… Pak bolest… A tma.\nPrudce otevíráš oči a bolest z podivného snu fyzicky cítíš (-1K). Dívka stále stojí před tebou a mlčky tě pozoruje.\n“Je čas jít, Mnesis!” připomene se znovu černovlasý muž, “už jsi jej potrápila dost!”\nDívka jen kývne.\n“Snad jsi nalezl, co jsi potřeboval,” věnuje ti ještě poslední úsměv.\nPak se přidá k podivnému průvodu, který za pravidelného bubnování vyrazí dál… A brzy ti zmizí z dohledu.",
        "decisions": {"40": "Pokračuj"},
        "events": {"life": -1},
    },
    {
        "id": 36,
        "paragraph": "Když vyslovíš své přání, dívka pomalu kývne hlavou:\n“Řekni mi… Jak se jmenuješ, člověče?”\nPřekvapí tě, že tě neoslovuje jako rytíře, ale cítíš, že je to tak… správně?\nV další chvíli tě pohltí záblesky vzpomínek.\nVidíš sám sebe, jako v zrcadle. Nemáš brnění, ani jiné oblečení, jsi… sám před sebou nahý. A tak, bez jakýchkoliv masek… Se ti zjevuje pravda.\nA ty vidíš své jméno a víš, že je to správné.\n(Vymysli si své jméno - právě sis na něj vzpomněl. Zapiš si ho do průvodní listiny do příslušné kolonky.)\nPrudce otevíráš oči. Před tebou stále stojí dívka z podivného průvodu a mlčky tě pozoruje.\n“Je čas jít, Mnesis!” připomene se znovu černovlasý muž, “už jsi jej potrápila dost!”\nDívka jen kývne:\n“Snad jsi nalezl, co jsi potřeboval,” věnuje ti ještě poslední úsměv.\nPak se přidá k podivnému průvodu, který za pravidelného bubnování vyrazí dál… A brzy ti zmizí z dohledu.\nS vypětím všech sil se zvedáš ze země a pozoruješ moře před sebou.",
        "decisions": {"39": "Co ti moře zjeví zjistíš otočením na"},
        "events": {"name": "debug_name"},
    },
    {
        "id": 37,
        "paragraph": "Když tiše vyslovíš své přání, dívka pomalu kývne hlavou:\n“Jaké je poslání tvého řádu, člověče?”\nPřekvapí tě, že tě neoslovuje jako rytíře, ale cítíš, že je to tak… správně?\nV další chvíli tě pohltí záblesky vzpomínek.\nNevidíš však řád, ani honosné sály hradů a zámků. Vidíš jen lazaret, či cosi podobného, kde pomáháš raněným a potřebným dostat se z nejhoršího.\nMůže to být ono poslání?\nPrudce otevíráš oči -  šťastná vzpomínka ti vlila do žil trochu sil (+1K). Před tebou stále stojí dívka z podivného průvodu a mlčky tě pozoruje.\n“Je čas jít, Mnesis!” připomene se znovu černovlasý muž, “už jsi jej potrápila dost!”\nDívka jen kývne.\n“Snad jsi nalezl, co jsi potřeboval,” věnuje ti ještě poslední úsměv.\nPak se přidá k podivnému průvodu, který za pravidelného bubnování vyrazí dál… A brzy ti zmizí z dohledu.\nS vypětím všech sil se zvedáš ze země a pozoruješ moře před sebou.",
        "decisions": {"39": "Co ti moře zjeví zjistíš otočením na"},
    },
    {
        "id": 38,
        "paragraph": "Zachraptíš, že si nepřeješ, aby se tě dívka na cokoliv ptala. Bolesti máš v sobě už tak až moc a cítíš, že by ti její slova jen způsobila další.\nDívka tě mlčky pozoruje, zdá se, jakoby chtěla něco říct -\n“Slyšela jsi ho, Mnesis,” zavolá na ni černovlasý muž z průvodu, “nic od tebe nechce. Tak už pojď!”\nDívka vrhne rychlý pohled přes rameno, pak se podívá znovu na tebe.\n“Možná je to tak lepší,” zašeptá, “co duše neví, srdce nebolí.”\nPak se přidá k podivnému průvodu, který za pravidelného bubnování vyrazí dál… A brzy ti zmizí z dohledu.\nS vypětím všech sil se zvedáš ze země a pozoruješ moře před sebou.",
        "decisions": {"39": "Co ti moře zjeví zjistíš otočením na"},
    },
    {
        "id": 39,
        "paragraph": "V uších ti doznívá bubnování záhadného bubeníka, které ostře kontrastuje s klidným šuměním moře před tebou.\nUž si říkáš, že jsi odsouzený k tomu, abys navždy dožil na tomto pobřeží…\nPak ti však něco vrátí naději.",
        "decisions": {"40": "Pokračuj"},
    },
    {
        "id": 40,
        "paragraph": "Cosi se objeví na obzoru.\nZacloníš si oči rukama, abys lépe viděl… A spatříš osamělou stojící postavu, která pohání malý člun jakýmsi dlouhým bidlem, o které se opírá. Neznámý je oděný v šedém plášti a je nejméně o hlavu větší, než ty. Nakonec loď přirazí až ke břehu, ale lodivod nevystupuje.",
        "decisions": {
            "41": "Můžeš na něj zaútočit otočením na",
            "42": "Pokud jej chceš požádat, aby tě převezl na druhou stranu, pokračuj na",
            "43": "Nebo můžeš jen čekat, zda-li přeci jen nezačne mluvit sám - pak jdi na",
        },
    },
    {
        "id": 41,
        "paragraph": "Jsi si jistý, že dokážeš loď vést přes moře i bez pomoci toho podivína. Proto tasíš meč a vrhneš se vpřed-\nLodivod se pohne až nelidsky rychle a jeho bidlo tě bolestivě zasáhne přesně do rány na boku, která tě pomalu zabíjí (-2K).\nS výkřikem ustoupíš, zatímco cítíš, že se ti tělem šíří zvláštní chlad.\nLodivodova tvář je stále zahalena kápí. Zdá se nevzrušen tím, co se právě stalo.",
        "decisions": {
            "42": "Pokud jej chceš požádat, aby tě převezl na druhou stranu, otoč na",
            "43": "Nebo můžeš jen čekat, zda-li přeci jen nezačne mluvit sám - pak otoč na",
        },
        "events": {"life": -2},
    },
    {
        "id": 42,
        "paragraph": "Na tvou prosbu postava v kápi jen kývne a natáhne před sebe kostnatou bledou ruku.\nRozevře dlaň a dlouhým prstem levačky na ní dvakrát ukáže.\nChápeš jeho gesto, ale s hrůzou si uvědomuješ, že při sobě nemáš žádné peníze.\nV tom se odkudsi nad tebou ozve zvuk, který jsi nemyslel, že ještě uslyšíš.",
        "decisions": {"44": "Pokud jsi dal havranovi najíst bobulí", "45": "Pokud ne"},
    },
    {
        "id": 43,
        "paragraph": "(-1K)\nČekání se mění v dlouhé minuty, zatímco tvé tělo umdlévá z utržených zranění.\nLodivod ale nevydá ani hlásku. Jediné, co ruší absolutní ticho, je šumění moře.",
        "decisions": {
            "41": "Pokud jsi to ještě neudělal, můžeš na lodivoda zaútočit",
            "42": "Nebo ho můžeš požádat, zda-li by tě nepřevezl na druhou stranu",
        },
        "events": {"life": -1},
    },
    {
        "id": 44,
        "paragraph": "Šumění moře a lodivodovo mlčení naruší hlasité krákání.\nStačí jen pár chvil, abys pochopil, že přilétá tvůj starý známý.\nSnese se k tobě a usedne ti na rameno, pak se přátelsky otře svou opeřenou hlavou o tvou tvář.\nNení to vůbec nepříjemné, uvědomíš si.\nHavran poposedne na tvém rameni a krátce pohlédne na lodivodovu stále nataženou dlaň. Teprve v tu chvíli si všimneš, že opeřenec cosi drží v zobáku.\nAniž by ti dal na výběr, černý opeřenec se předkloní a do natažené dlaně tiše dopadne kulatá mince. Je bohatě zdobená krutými výjevy z bitvy, kde stvůry bez tváře trhají na kusy spořádané rytířské šiky.\nCítíš, jak se v tobě zvedá vztek, když si uvědomíš, jak hroznou porážku rytíři z Alby utrpěli…\nV druhé chvíli lodivod sevře minci v dlani a poodstoupí, abyste mohli i s havranem nastoupit.\nTrvá to celé jen několik chvil… A pak již vyplouváte přes Moře Vzpomínek do velké dálky.",
        "decisions": {"47": "Co skrývá se dozvíš, když otočíš na"},
    },
    {
        "id": 45,
        "paragraph": "Šumění moře a lodivodovo mlčení naruší hlasité krákání.\nStačí jen pár chvil, abys pochopil, že je to tvůj starý známý.\nHavran ti na rameno, i když máš pocit, že dvakrát nadšený z toho není.\nNení to vůbec nepříjemné, mít ho u sebe, uvědomíš si.\nPták poposedne na tvém rameni a krátce pohlédne na lodivodovu stále nataženou dlaň.\nPak zavrtí svou malou hlavou a žalostně zakráká, jakoby se omlouval.\nS rostoucím strachem si uvědomíš, že lodivod žádá minci za převoz, jako v mnoha legendách, které jsi slyšel - ty ji však nemáš.\nJsi snad odsouzen zemřít na tomto pobřeží?",
        "decisions": {"46": "Otoč na"},
    },
    {
        "id": 46,
        "paragraph": "Jakoby slyšel tvé pochyby, lodivod vytáhne ze záhybů svého pláště zakřivenou dýku a rychlým pohybem ti ji přiloží k dlani.\nJímá tě hrůza, ale přesto kývneš.\nLodivod zaboří hrot své dýky do tvé dlaně a táhne, zatímco bolestí zatínáš zuby (-1K).\nOhromeně sleduješ, co se děje - jak dýka pije tvou krev, tak se v lodivodově levé dlani vytváří krvavá mince.\nKdyž je se svou prací hotov, lodivod sevře krvavou minci v dlani a nechá tebe i s havranem nastoupit.\nJeště, než vyplujete, kývne váš záhadný průvodce k tvé ruce.\nŽasneš - ač bolest stále cítíš, na dlani není ani stopy po zranění.\nPak lodivod odrazí svým pádlem od břehu a vy tak konečně vyrážíte přes Moře vzpomínek.",
        "decisions": {"47": "Co skrývá se dozvíš, když otočíš na"},
        "events": {"life": -1},
    },
    {
        "id": 47,
        "paragraph": "Necháváš se i s havranem unášet na vlnách širého moře, zatímco mlčenlivý lodivod udává směr vaší loďce. Ačkoliv je malá, brázdí modrou dálku odhodlaně a s nepřirozenou rychlostí.\nNa pár okamžiků si dovolíš zavřít oči a nechat bojiště za sebou.\nCítíš, že odpovědi jsou nedaleko, jen překonat tohle nekonečno, nenechat se zdeptat minulostí.\nO chvíli později tě přepadne strach. Co když jsou odpovědi horší, než tyto…vlny neznáma?\nJako v odpověď na tvou nevyslovenou obavu se loď zastaví a jen líně driftuje na hladině.\nHavran tázavě zakráká, ale když otevřeš oči, spatříš jen černo v lodivodově kápi.\nJeho bledá, kostnatá ruka ukazuje nejdříve doleva a poté doprava.\nV každém směru vidíš zajímavý úkaz.\nPo své pravici vidíš na malém ostrovu plném zpřelámaných kopí stát nemrtvého legionáře v rudém tabardu.\nPo své levici pak vidíš další ostrov, na kterém je jakási miniaturní kamenná stavba.",
        "decisions": {
            "48": "Pokud chceš, aby tě lodivod zavezl na malý ostrov napravo, kde stojí nemrtvý legionář v rudém tabardu, otoč na",
            "53": "Pokud chceš, aby tě zavezl na kamenitý mys vlevo, kde vidíš podivnou kamennou stavbu, pokračuj na",
        },
    },
    {
        "id": 48,
        "paragraph": "Lodivod poslechne tvé přání a tiše pobídne loď směrem, který jsi zvolil.\nJak se blížíte, vidíš jednoho z legionářů, kteří zpustošili vaše šiky.\nTento je ale přesto pozoruhodný - má široká ramena a v rukou třímá obří palcát. Vzpomínáš na to, jak padl Leantes - byl to právě tento zplozenec Pekel, který způsobil smrt tvého přítele.\nJak plujete blíže, Legionář vyrazí hrdelní smích:\n“Od minulosti se neodchází snadno, že? Ale pozor, aby tě nezničila!”\nJak loď přirazí ke břehu, přeskakuješ bočnici loďky a vyrážíš se utkat s tímto mocným protivníkem.\nOpatrně se k sobě blížíte, zatímco tvůj havran vzlétne do vzduchu a ustrašeně kráká.",
        "decisions": {
            "49": "Pokud chceš  zaútočit jako první",
            "50": "Pokud chceš nechat legionáře, aby boj zahájil",
        },
    },
    {
        "id": 49,
        "paragraph": "Nezaváháš ani na okamžik a přejdeš rovnou do útoku. Bodáš s lehkostí a přesto rozhodně, rozhodnut pomstít smrt přítele.\nRudý legionář se vrhne před, rozhodnut tě jednou provždy zničit - jeho předchozí úsměv je ale ten tam, jeho arogance umlčena tvým odhodláním\n\nHoď dvěma kostkami. Vzhledem ke svému odhodlání můžeš výsledek 1x opakovat, druhý hod ale musíš přijmout.",
        "decisions": {
            "4": "Pokud je výsledek menší než",
            "51": ", otoč na",
            "52": "Pokud je výsledek vyšší, otoč na",
        },
    },
    {
        "id": 50,
        "paragraph": "Opatrně vyčkáváš na první útok svého protivníka.\nTen potěžká svůj palcát a odfrkne si.\n“Ani s vlastní minulostí se neumíš poprat,” zavrčí, “pak je celá tahle cesta k ničemu, že? Tak proč ji neskončit teď a tady!”\nS tím vykročí dopředu - a ty mu vyjdeš vstříc.\nJe to děsivý protivník a ty cítíš, že nebojuješ jen proti němu… Ale i proti svému vlastnímu strachu.\nHoď dvěma kostkami.",
        "decisions": {
            "51": "Pokud je výsledek menší než 6",
            "52": "Pokud je výsledek vyšší",
        },
    },
    {
        "id": 51,
        "paragraph": "Dávní učenci říkají, že od minulosti se oprostíme jen těžko.\nA někdy je to jednoduše nemožné.\nKdyž tě ocelový palcát tvého soka zasáhne vší silou do hlavy, padneš k zemi jako podťatý.\nHledal jsi odpovědi… Ale minulost jsi za sebou nechat nedokázal.\nTvá cesta i tvůj život končí tragický zde, na ostrově v Moři vzpomínek.\n\n",
        "decisions": {},
        "events": {"death": 1},
    },
    {
        "id": 52,
        "paragraph": "Necháš obrovitý palcát prosvištět vzduchem a pak se vrhneš vpřed, zbraň připravenou.\nRudý Legionář překvapeně vydechne, když se mu tvůj meč zaboří do černého srdce.\n“Tak přece ses dokázal od minulosti… oprostit,” zašeptá nemrtvý, než padne.\nKlečíš u jeho mrtvoly a hlavou ti běží vzpomínky na Leanta.\nPomstil jsi ho se ctí.\nHavran kdesi nad tebou vesele zakráká a snese se ti zpět na rameno.\nS pocitem, že šero kolem tebe se opět trochu protrhalo se vracíš na loď.\nPokud chceš lodivoda vyzvat, aby tě zavezl ještě na druhý ostrov, kde je jakási stavba, otoč na 53, pamatuj ale, že sem se již vrátit nemůžeš.",
        "decisions": {
            "57": "Pokud nechceš ztrácet další čas a chceš se nechat převézt přes moře - pak otoč na"
        },
    },
    {
        "id": 53,
        "paragraph": "Lodivod mlčky poslechne tvé přání, cesta je však delší, než se původně zdálo a tvé rány se znovu připomenou (-1K).\nKdyž konečně dorazíte na břeh, stoupáš i s havranem do malého kopce, kde na samém vrcholu vidíš zvláštní úkaz.\nKdosi sem na plochý kámen umístil zdrobnělinu jakéhosi výjevu, který je však nekompletní.\nScéna, na kterou shlížíš, znázorňuje jakousi strž, na jejímž dně vidíš zaschlou krev. Opodál spatříš stát tři figury - princeznu, jakéhosi poutníka a figuru rytíře, která se ti až nápadně podobá.\nKdyž přemýšlíš, jak tento hlavolam vyřešit, všimneš si na dně miniaturní strže tři značek. První značka na volném prostranství má tvar obličeje zkřiveného výkřikem. Druhá, obklopená krví, znázornuje\u200b zranění. Třetí pak je značka pomoci - při bližším zkoumání si všimneš, že nad tou je umístěno několik padajících kamenů, které neznámý umělec chytře přichytil ke stěně strže.\nCítíš, že pokud umístíš figury správně, bude ti odhaleno tajemství celého tohoto výjevu.\nPokud chceš umístit princeznu na značku pomoci, rytíře na značku zranění a poutníka na značku výkřiku, otoč na 54\nPokud chceš umístit na značku pomoci rytíře, ke zranění přiřadit poutníka a princeznu na znamení výkřiku, dej se na 55\nPokud chceš umístit na značku pomoci poutníka, na značku zranění princeznu a na značku výkřiku rytíře, zkus to otočením na 56\nPřípadně můžeš tento výjev prostě opustit a požádat lodivoda, aby tě odvezl na ostrov s nemrtvým legionářem v rudém tabardu, který čeká na 48, pokud jsi tam už nebyl.",
        "decisions": {
            "57": "Nebo se můžeš nechat vést lodí jednoduše dál přes moře a na"
        },
        "events": {"life": -1},
    },
    {
        "id": 54,
        "paragraph": "Rozmístíš figury dle nejlepšího uvážení a čekáš. Po chvíli slyšíš, jak odkudsi přichází burácivý rachot, jako by odněkud padala kamenná lavina… A v další chvíli vykřikneš bolestí, jak tě neviditelná síla málem rozdrtí o zem (-1K). Vrávoravě vstáváš, ačkoliv tvé tělo protestuje…\nKdyž shlížíš na scénu ve strži, figurky jsou poházené po celém pódiu onoho zvláštního divadla.",
        "decisions": {
            "55": "Pokud chceš umístit na značku pomoci rytíře, ke zranění přiřadit poutníka a princeznu na znamení výkřiku, otoč na",
            "56": "Pokud chceš umístit na značku pomoci poutníka, na značku zranění princeznu a na značku výkřiku rytíře, otoč na",
            "48": "Případně můžeš tento výjev prostě opustit a požádat lodivoda, aby tě odvezl na ostrov s nemrtvým legionářem v rudém tabardu - pak pokračuj na",
            "57": "Nebo se můžeš nechat vést lodí jednoduše dál přes moře na",
        },
        "events": {"life": -1},
    },
    {
        "id": 55,
        "paragraph": "Rozmístíš figurky tak, jak myslíš, že se celý výjev odehrál a čekáš. Po chvíli slyšíš rachot, který stále nabývá na síle, jakoby odněkud padala hromada balvanů. Připravíš se uskočit, když v tu chvíli scéna před tebou ožije - figury, které jsi rozmístil, ti přehrávají svůj příběh.\nSlyšíš sténat poutníka, který se zřejmě zranil při pádu do strže. K němu odhodlaně běží rytíř, připraven mu pomoci, následován princeznou. Ta ho varuje před padajícími kameny, ale rytíř, hluchý ve svém odhodlání, na varování nedbá. O chvíli později pak rytíře zavalí kameny. Do ticha výjevu slyšíš jen zděšené volání princezny, která volá o pomoc - a proklíná noc, která záchranu jistě zdrží.\nV dalším okamžiku figury i celá scéna ztuhne, příběh tohoto výjevu je dokončen.\nMáš nepříjemný pocit, že ti tato scéna nebyla vyjevena náhodou. Havran na tvém rameni poplašeně kráká. Přesto cítíš příval nového odhodlání.\n(Pokud jsi na tomto ostrově ztratil nějaké body Kondice, opět si je obnov. Na tento ostrov se již nemůžeš vrátit.)\nSestupuješ zpátky k lodi, doprovázen svým opeřeným společníkem.",
        "decisions": {
            "48": "Chceš-li požádat lodivoda, aby tě odvezl na ostrov s nemrtvým legionářem v rudém tabardu, otoč na",
            "57": "Nebo se můžeš nechat vést lodí jednoduše dál přes moře",
        },
    },
    {
        "id": 56,
        "paragraph": "Rozmístíš figury dle nejlepšího uvážení a čekáš. Po chvíli slyšíš, jak odkudsi přichází burácivý rachot, jako by odněkud padala kamenná lavina… A v další chvíli vykřikneš bolestí, jak tě neviditelná síla málem rozdrtí o zem (-1K). Vrávoravě vstáváš, ačkoliv tvé tělo protestuje…\nKdyž shlížíš na scénu ve strži, figurky jsou poházené po celém povrchu onoho zvláštního divadla.\nJak se rozhodneš, rytíři z Alby?",
        "decisions": {
            "54": "Pokud chceš umístit princeznu na značku pomoci, rytíře na značku zranění a poutníka na značku výkřiku, otoč na",
            "56": "Pokud chceš umístit na značku pomoci poutníka, na značku zranění princeznu a na značku výkřiku rytíře",
            "48": "Případně můžeš tento výjev prostě opustit a požádat lodivoda, aby tě odvezl na ostrov s nemrtvým legionářem v rudém tabardu, když otočíš na",
            "57": "Nebo se můžeš nechat vést lodí jednoduše dál přes moře",
        },
        "events": {"life": -1},
    },
    {
        "id": 57,
        "paragraph": "Lodivod tě veze dál, přes moře, které se zdá nekonečné.\nDovolíš si zavřít oči a oddat se svým myšlenkám.\nCo tě čeká na druhé straně?\nJe to opravdu konec, jak praví legendy a zvěsti?\nA pokud ano… Až se tě Smrt zeptá, kdo před ní přichází…\nBudeš na konci schopen říct, jak to celé začalo?\nZ myšlenek a pochyb tě vyruší sotva znatelný náraz.\nOtevřeš oči a spatříš, že jste přirazili ke břehu.\nPřed tebou je široké pobřeží, pokryté šedým pískem, na kterém několik set sáhů před tebou stojí osamělá postava - jediný opěrný bod v celé té nekončící šedi.\nHavran na tvém rameni nervozně přešlápne, zakrákat se však zřejmě neodváží.",
        "decisions": {
            "58": "Pokud se chceš lodivoda zeptat, kam tě další cesta zavede, otoč na",
            "59": "Pokud ne, nezbyde ti, než\u200b vyrazit i s havranem vpřed na",
        },
    },
    {
        "id": 58,
        "paragraph": "Lodivod na tvou otázku odpoví hlasem studeným jako led, slova vychází přímo z temnoty jeho kápě:\n“Nevezme tě nikam, kde jsi už nebyl, poutníče.”\nTa odpověd v tobě vzbudí jen víc otázek.\nS tichým díky tak vystupuješ na břeh, rozhodnut postavit se neznámému.",
        "decisions": {"59": "Pokračuj"},
    },
    {
        "id": 59,
        "paragraph": "Ve stejné chvíli, kdy uděláš svůj první krok, lodivod i se svou lodí odrazí od břehu - a během chvíle zmizí v mlze, která obklopuje pobřeží.\nKráčíš po šedém písku i s okřídleným společníkem, který však nezvykle mlčí, jakoby sám nevěděl, co se chystá.\nBrzy máš pocit, že se jemný písek pod tvými kroky samovolně přesouvá. Snažíš se proti té neznámé síle bojovat, ale marně - nakonec ti nezbyde nic, než se nechat unášet.\nPo několika minutách se tak ocitáš u neznámé postavy, kterou jsi zpozoroval už z lodi.\nPostava je k tobě otočena zády - je oděná v černých brokátových šatech, které jsou vyšívané stříbrnou nití. Vlasy má světlé, jako by byly utkány sluncem.",
        "decisions": {
            "60": "Můžeš využít své šance a na neznámého zaútočit",
            "61": "V takovém případě pokračuj zde",
        },
    },
    {
        "id": 60,
        "paragraph": "Rozhodneš se nečekat a vykročíš - v tu chvíli se postava otočí a vrhne se na tebe holýma rukama, kterýma do tebe buší, jak tě její rychlá reakce překvapí.\n“Ty zrádce! Zrádce! Jak jsi to mohl udělat!” křičí na tebe mladá žena v černých honosných šatech.",
        "decisions": {"62": "V tom případě otoč na"},
    },
    {
        "id": 61,
        "paragraph": "Sprosté obviňování té osoby skončí ve chvíli, kdy jí tvůj meč probodne srdce.\nŽena vytřeští oči bolestí:\n“Pravda je tam… hluboko,” zašeptá a ukáže prstem k místu nedaleko za sebou, kde v zemi zeje široká strž.\n“Pravda a také vinc-“\nPak žena podklesne v kolenou a sveze se do šedého písku.\nHavran na tvém rameni žalostně zakráká…\nTy ho ale neposloucháš.\nUděláš krok vpřed… a podivný písek tě již vede směrem k průrvě.\nPokračuj na 71\n\n",
        # \nPokud chceš využít faktu, že je neozbrojená a skončit to, jdi na 61\nNebo do sebe můžeš nechat dál bušit - nezdá se, že by neznámá byla schopna poslouchat cokoliv, co bys ji chtěl případně říct. V tom případě otoč na 62\n\n
        "decisions": {"71": "Pokračuj na", "99": "ERROR FIX THIS PARAGRAPH"},
    },
    {
        "id": 62,
        "paragraph": "Necháš drobné pěsti, aby tě zasahovaly znovu a znovu, sílu ublížit ale nemají.\nJak si na tobě žena vybíjí zlost, její rozhořčení postupně upadá:\n“Slíbils mi to! Slíbil! Proč jsi mi lhal? Proč?”",
        "decisions": {
            "63": "Pokud se jí chceš zeptat, cos jí slíbil, otoč na",
            "68": "Pokud jí chceš jen dál nechat, aby si dělala, co bude chtít, jdi na",
            "61": "Případně můžeš stále vše skončit svým mečem otočením na",
        },
    },
    {
        "id": 63,
        "paragraph": "“Cos mi slíbil?” zeptá se překvapeně žena, “Přece… přece, že budeš v pořádku!”\nPak se ti vrhne kolem krku.\n“O-omlouvám se,” vzlyká, “Chtěla jsem tě varovat, ale… Nestihla jsem to…”",
        "decisions": {
            "64": "“O-omlouvám se,” vzlyká, “Chtěla jsem tě varovat, ale… Nestihla jsem to…”\nPokud ji chceš odpustit",
            "68": "Pokud chceš jen dál mlčet",
        },
    },
    {
        "id": 64,
        "paragraph": "Když jí odpustíš, žena padne dojetím na kolena do šedého písku.\n“Děkuji… Je mi to moc líto,” zašeptá.\nPak se ti podívá do očí a usměje se:\n“Víš, kdo jsem?”\nPokud myslíš, že víš, jak se žena jmenuje, sečti písmena jejího jména - tím získáš první číslo odkazu, na který otočit. Druhé je pak je o 1 vyšší, než číslo první (např. Klára - první číslice 5, druhá 6, tj. odkaz 56) Písmeno CH případně počítej jako dvě písmena.",
        "decisions": {
            "68": "Pokud její jméno neznáš, nebo odkaz, na který jsi otočil, nebude dávat smysl, nebo chceš jednoduše mlčet, otoč na"
        },
    },
    {
        "id": 67,
        "paragraph": "Při zvuku svého jména se dívce rozzáří oči:\n“Ano, to je moje jméno,” kývne, “jsem Chiara, což znamená dcera světla. Tomu ses přeci tolikrát smál.”\nMyšlenky ti v hlavě kreslí obraz tebe a této ženy. Oba jste zabráni do práce v jakémsi lazaretu. Potkaly se vaše ruce opravdu, nebo je to jen sen?\nDívka pomalu vstane a odhrne si z čela pramen světlých vlasů:\n“Nemohu jít s tebou,” posmutní, “protože toto je jen a jen tvá cesta.”\nHavran při jejích slovech smutně zakráká, ale dívka se na něj povzbudivě usměje:\n“Vím, že jsi unavený, ptáčku. Ale jak jsem řekla… je to jen a jen tvá cesta. Musíš vydržet… Až do konce. Bez ohlížení. Bez zastavení.”\nChceš se té ženy zeptat na nespočet věcí, ale jen zavrtí hlavou:\n“Já… jsem jen část příběhu. Musíš… dojít na konec. Tam čeká tvé… vincero. A pak se ke mně vrať, chceš-li.”\nS tím ti pokyne rukou kamsi za sebe, kde vidíš ústí jakési strže.",
        "decisions": {
            "71": "Pokud se chceš i s havranem vydat ke strži, otoč na",
            "70": "Pokud chceš Chiaře říct, že s ní prostě zůstaneš zde",
        },
    },
    {
        "id": 68,
        "paragraph": "“Myslela jsem si to,” sykne tiše žena, zatímco ji jen mlčky pozoruješ.\nHněv z ní již zcela vyprchal a zůstává jen… smutek?\n“Tak tedy jdi,” otře si po chvíli slzy a pokyne ti rukou kamsi za sebe, kde nedaleko vidíš jakousi širokou strž.\n“Tam čekají odpovědi,” řekne žena tiše, “stejně jako já… budu čekat zde. Snad najdeš své… Vincero.”",
        "decisions": {
            "70": "”\nPokud chceš zůstat se ženou, otoč na",
            "71": "Nebo můžeš i s havranem vyrazit ke strži, otoč na",
        },
    },
    {
        "id": 69,
        "paragraph": "Když neznámou oslovíš, prudce se k tobě otočí a ty vidíš ženský obličej, zkřivený hněvem a nenávistí.\n“Jak se opovažuješ mě oslovit? Po tom všem! Ty zrádce! Zrádce! Jak jsi to mohl udělat?”\nS těmi slovy se k tobě bohatě oděná žena vrhne a začne do tebe bušit drobnými pěstmi.\nPokud chceš využít faktu, že je neozbrojená a skončit to svou čepelí, otoč na 61\nNebo do sebe můžeš nechat dál bušit - nezdá se, že by neznámá byla schopna poslouchat cokoliv, co bys ji chtěl případně říct.",
        "decisions": {"62": "V tom případě otoč na"},
    },
    {
        "id": 70,
        "paragraph": "“Zůstat? Zůstat tu se mnou? To bys… To bys vážně chtěl?”\nRozhodně přikývneš.\nAčkoliv je kolem vás šedá poušť, tahle žena je pro tebe nadějí, plamínkem svíce v temnotách.\nŽena tě obejme a její náruč hřeje.\n“Možná, možná bys měl jít…”\nPohladíš ženu po tváři a tím její protesty nadobro utišíš.\nPrsty, které tě ještě před chvílí bily do tváře, tě žena hladí ve vlasech.\nSnad… snad to takhle mělo od začátku skončit.\nČas je ale neúprosný a z ran ti vytéká víc a víc krve.\nHavran se ti smutně usadí na rameni…\nNapadá tě, že některé věci se nejspíš nikdy nedozvíš.\nAle…\nPak ztratíš až příliš mnoho krve.\nTvůj příběh zde sladkobolně končí, v náruči té, která byla tvou nadějí.\n\n",
        "decisions": {},
        "events": {"death": 1},
    },
    {
        "id": 71,
        "paragraph": "Kráčíš ke strži a tvé kroky nikdo a nic neruší.\nCítíš, jak ti do zad praží neviditelné slunce a jazyk se ti lepí na patro.\nPak náhle z leva zaslechneš zoufalé volání.\nOtočíš se - a spatříš rytíře Leanta, jak bojuje proti přesile nemrtvých legionářů.\nMáš jen několik vteřin, abys zareagoval - je jasné, že Leantes v boji brzy podlehne.",
        "decisions": {
            "72": "Pokud mu chceš vyrazit na pomoc",
            "73": "Pokud chceš pokračovat dál ke strži a křiku přítele si nevšímat",
        },
    },
    {
        "id": 72,
        "paragraph": "Vyběhneš k bojujícím a postavíš se bok po boku rytíře Leanta.\nPřipravuješ se odrazit rány nemrtvých, když ti náhle v boku vybuchne strašlivá bolest.\nZděšeně se otáčíš a vidíš, jak ti z rozšklebené rány ční Leantův meč.\nZ úst ti vytryskne pramínek krve a síly tě rychle opuští. Leantes se ti mění před očima - kde byla dříve pohledná tvář, je nyní rozšklebená lebka nemrtvého válečníka.\n“Tak přece, přece lest vyšla!” směje se hrdelním smíchem, “Popelavý král ulovil i posledního z Alby…”\nChceš bojovat, ale… sil už se jednoduše nedostává.\nTvůj život zde tragicky končí.\n\n",
        "decisions": {},
        "events": {"death": 1},
    },
    {
        "id": 73,
        "paragraph": "(-1K)\nVyrážíš dál a když se ohlédneš, bojující jsou pryč - vše byla jen iluze, která tě měla nalákat.\nCesta ke strži trvá nakonec jen něco přes hodinu.\nZnavený a krvácející stojíš na kraji strže. Když pohlédneš dolů, na okamžik tě zachvátí hrůza.\nTam, hluboko pod tebou, na samém dně propasti, čeká Popelavý král.\nJeho majestátní postava dosahuje bezmála dvou metrů, rozložitá ramena i hruď má krytá černou zbrojí, opírá se o obouruční meč.\nKdyž ho spatří tvůj havran, přidušeně zakráká.\nV duchu prokleješ jeho slabost, ale je už příliš pozdě.\nPopelavý král zvedne hlavu, krytou jen kroužkovou kápí. Když tě spatří, tiše kývne:\n“Tady to začalo a tady to i skončí, člověče.”\nJen slyšet ten hlas je jako čelit samotné smrti.\nCítíš, jak z tebe jeho slova vysávají životní sílu.\n“Přijď ke mně. Mám co nabídnout.”\nChvíli přemítáš o jeho slovech, ale Popelavý král se rozesměje kousavým smíchem a tobě ve zraněném boku vybuchne obrovská bolest.\n“Tady to začalo a tady to i skončí. Je jen na tobě, jak,” zahřímá vládce nemrtvých legií.\nPři těch slovech tvůj havran bolestivě zakrákorá a slétne ti z ramene.\nVidíš, že je zraněný, stejně jako ty - pírka má tu a tam od krve, občas se zdá, že jsou ohořelá…\nVšimneš si, že do stěny strže jsou vytesány schůdky.\nJe ti ale jasné, že sestup dolů bude namáhavý a vyžádá si možná i poslední zbytky sil, které v sobě máš. Nadarmo tu ale nezemřeš.",
        "decisions": {
            "74": "Pokud chceš havranovi poděkovat a jít dolů sám, otoč na",
            "75": "Pokud jej chceš vysadit znovu na své rameno a pokračovat ke králi společně, otoč na",
        },
        "events": {"life": -1},
    },
    {
        "id": 74,
        "paragraph": "Poděkuješ havranovi, za jeho služby a vysvětlíš mu, že dál musíš sám.\nČerný pták tě chvíli pozoruje svýma korálkovýma očima - pak zakráká, jako bys mu zasadil smrtelný úder a klesne k zemi.\nKdyž jej překvapeně pozoruješ, zjistíš, že je mrtvý.\nJeho ztráta tě zasáhne víc, než jsi čekal.\nProč tě vlastně doprovázel?\nTakové a podobné myšlenky ti víří hlavou, když sestupuješ po vytesaných schodech dolů do propasti, vstříc svému osudu.",
        "decisions": {"76": "Pokračuj na"},
    },
    {
        "id": 75,
        "paragraph": "Ačkoliv je tvůj okřídlený společník zraněný, nedokážeš jej nechat za sebou. Skloníš se k němu a nabídneš mu svou dlaň, aby si na ni vylezl.\nHavran tě jen tiše pozoruje svýma korálkovýma očima…\nPak ale tvou nabídku přijímá a i když tu a tam bolestivě kráká, o pár chvil později ti znovu sedí na rameni.\nBude tohle hloupé gesto vůbec k něčemu?\nTakové a podobné myšlenky ti víří hlavou, když sestupuješ po vytesaných schodech dolů do propasti, vstříc svému osudu.",
        "decisions": {"76": "Pokračuj na"},
    },
    {
        "id": 76,
        "paragraph": "Jak klesáš níž a níž po úzkých schodech, slyšíš nad sebou ryk stovek hrdel. Odvážíš se vzhlédnout - a vidíš nespočet nemrtvých legionářů, kteří se přišli pokochat, jak jejich vládce setne posledního z Alby. Jasně také vidíš, že se tma kolem znovu prohlubuje.\nPevně stiskneš jílec svého meče.\nCopak jsi celou tu dobu překonával svá zranění, jen abys teď padl?\nPřesto tě jedna část celé cesty znepokojuje - kdybys zemřel, tvá pouť by skončila.\nCo když zvítězíš? Co pak?\nKdyž sestoupíš na dno strže, Popelavý král tě pozdraví pozvednutím svého meče, kterému se kolem ostří mihotá namodralý plamen.\n“Řekl jsem, že mám nabídku a tu ti také předložím,” zaburácí hlas pána nemrtvých.\n“Mohu to skončit jediným úderem svého meče. Smrt přijde rychle. Nikdy se nedozvíš pravdu, ale bude ti dovoleno dožít v téhle milosrdné lži.”\nBeze slova čekáš na druhou část návrhu.\n“Pokud zkřížíme meče, poznáš neskonalou bolest. Pravda se do tebe zahryzne plnou silou. Lež pomine, ale tělo smrtelníka nedokáže takovou bolest přijmout.“\nUvědomíš si, že na pár úderů srdce utichnou i skandující hordy nemrtvých.",
        "decisions": {
            "77": "Pokud chceš, aby Popelavý král vše ukončil jediným úderem meče, otoč na",
            "78": "Pokud ne, nezbývá ti, než bojovat otočením na",
        },
    },
    {
        "id": 77,
        "paragraph": "Popelavý král se zdá být tvou odpovědí na okamžik překvapen, pak ale kývne hlavou.\n“Snad je to tak lepší, pravda.”\nPak pozvedne svůj meč, který zaburácí modrým plamenem a z jeho ostří vylétne blesk, který tě zasáhne do hrudi.\nPopelavý král nelhal.\nSmrt přijde rychle a necítíš téměř vůbec nic.\nTvé dobrodružství zde končí.\n\n",
        "decisions": {},
        "events": {"death": 1},
    },
    {
        "id": 78,
        "paragraph": "Popelavý král potřese hlavou a hotoví se k boji.\nZ vrchu slyšíš hrdelní křik jeho pohůnků.\nKdyž se k tobě blíží tvůj protivník, všimneš si, že se jeho tvář začíná měnit.\nRysy démonického krále se přesouvají, až utvoří tvář, kterou důvěrně znáš - hledíš sám na sebe!\nTa proměna tě rozechvěje až do morku kostí.\nCo to znamená? Že by snad-\nV druhé vteřině je ale Popelavý král u tebe a jeho meč opíše smrtící oblouk modrého plamene - máš pocit, že slyšíš, jak sténá samotný vzduch, kterého se démonická čepel jen dotkla.\nJeho ránu na poslední chvíli vykryješ vlastní zbraní - s hrůzou však pozoruješ, jak modravé plamínky přeskočí na tvou zbraň a rychle se šíří po tvé zbrani dál, až vykřikneš bolestí, když tě plameny démonické zbraně začnou požírat.\nNeschopen udržet zbraň padáš na kolena, ale bolest nepřestává a když zachvátí tvou tvář, propadneš se kamsi hluboko do vzpomínek.\nHoď dvěma kostkami a sečti obě čísla. Pokud víš, jak se jmenuje princezna, přičti 8 k výsledku, který ti padl.\nPokud je výsledek 10 a menší, magický oheň tě zahubí a tvůj příběh zde tragicky končí.",
        "decisions": {"99": "Pokud je výsledek menší", "79": "Pokud je výsledek vyšší"},
    },
    {
        "id": 79,
        "paragraph": "Princezna… Vzpomínky na tu milou tvář a její zvoniví smích, tam kdesi v lazaretu… Ne, nebyl to lazaret. Byla to nemocnice, uvědomíš si. Vaše setkání a srdce, která na okamžik vynechala svůj pravidelný rytmus.\nPrincezna… Chiara. Ano, to je její jméno.\nTolik ses smál, že její jméno znamená Dcera světla.\nŘíkával jsi, že světlo se může vždy hodit.\nA ona… ano, milovala tě.\nByla tvým světlem, až do té chvíle, než…\nBolest magických plamenů je téměř nesnesitelná, stravuje tě a hrozí proměnit tě v prach.\nMezi vlnami agonie si všimneš, že tě Popelavý král soustředěně pozoruje.\nChceš znovu vyrazit do útoku… Ale zasáhne tě další vlna bolesti.\nDalší vlna pravdy, která nezná slitování.\nHoď dvěma kostkami a sečti obě čísla. Pokud sis během své cesty vzpomněl na své vlastní jméno, přičti 8 k výsledku, který ti padl.",
        "decisions": {
            "99": "Pokud je výsledek 10 a mnenší",
            "80": "Pokud je výsledek vyšší",
        },
    },
    {
        "id": 80,
        "paragraph": "Princezna… Chiara… A ty.\nJediné poslání, oba jste mu plně oddáni.\nPomáhat potřebným.\nHlášení, které vás žene do akce.\nJen ty a ona, zbytek vaší skupiny je příliš daleko, aby zasáhli včas…\nZnovu tě z myšlenek vytrhne bolest. Tvůj sok má v pohledu znepokojení, které se mísí s obdivem…\nVzpomínky tě vedou ke zbytku vaší skupiny.\n“Jsou pro nás mrtví, nedokážou nám pomoci, nedostanou se sem včas,” tak to tehdy říkala Chiara… ano.\nAno, tak to bylo.\nAle co byli zač?\nHoď dvěma kostkami a sečti obě čísla. Pokud jsi během svého dobrodružství napsal na hroby dvě cizí jména, přičti 8 k výsledku, který ti padl.",
        "decisions": {
            "81": "Pokud je výsledek 10 a menší, magický oheň tě zle popálí (-2K), pokud stále žiješ, pokračuj",
            "81": "Pokud je výsledek vyšší, i tentokrát odoláš ničivé síle magického ohně - pokračuj na",
        },
    },
    {
        "id": 81,
        "paragraph": "Vzpomínáš si.\nBývalo by tak špatné na ty dva počkat? Stejně jako ty a Chiara byli zdatní záchranáři…\nUrčitě by alespoň vybrali do vozu lepší hudbu, než ona.\nOperní árie? Vážně?\nAle to už je jedno - nečekali jste a vyjeli jste na pomoc jen ve dvou.\nTys byl ten, který rozhodl, že čekat na posily nemá cenu - a vysloužil sis za to od ní letmý polibek.\n“Můj rytíři,” šeptala ti sladce.\nPak to ale všechno dopadlo špatně…\nAle jak?\n“Jak?”\nTa slova vykřikneš tak hlasitě, až se probereš ze vzpomínek a náhle máš pocit, jak nad tebou plameny ztrácí moc.\nPopelavý král zalapá po dechu, jakoby snad poprvé zapochyboval o svém vítězství.\nZavřeš oči, abys pomohl vzpomínkám.\nStačí už tak málo, aby vše zapadlo na své místo.\nOdhoď lež, člověče.\nJak ses do tohoto podivného světa opravdu dostal?",
        "decisions": {
            "82": "Pokud myslíš, že jsi spadl z okraje strže, otoč na",
            "83": "Pokud myslíš, že na tebe byl spáchán atentát",
            "84": "Pokud myslíš, že tě zavalily padající kameny",
        },
    },
    {
        "id": 82,
        "paragraph": "Pokoušíš se vzpomenout, jakým způsobem ses v této podivné pláni bytí vlastně ocitl, ale marně.\nPád do strže je jen mylná představa, ale ve svých myšlenkách ho prožiješ, jakoby byl reálný.\nTakový pád by nepřežil nikdo.\nSmrt si tě vezme milosrdně rychle.\nTvůj příběh tragicky končí zde, na samé hranici vítězství.\n\n",
        "decisions": {},
        "events": {"death": 1},
    },
    {
        "id": 83,
        "paragraph": "Snažíš se vzpomenout, jak ses v této podivné pláni bytí vlastně ocitl, ale marně.\nAtentát je jen mylná představa, ale ve svých myšlenkách ho prožiješ plnou silou.\nRány, které ti zasadí vlastní mysl, mají stejnou sílu, jako rány reálné.\nSmrt si tě vezme milosrdně rychle.\nTvůj příběh tragicky končí zde, na samé hranici vítězství.\n\n",
        "decisions": {},
        "events": {"death": 1},
    },
    {
        "id": 84,
        "paragraph": "Otevíráš oči a upřeně hledíš sám na sebe. Dobře však víš, že si tvou tvář nasadil Popelavý král.\nKdyž pomalu vstáváš a třímáš v ruce svůj meč, magické plameny na tvém těle rychle pohasínají.\nVzpomínky tě vedou na okamžik zpět - do chvíle, kdy jste s Chiarou dorazili pomoci zraněnému cestovateli do hluboké strže - a na tvé vlastní odhodlání ukázat své milé, jak jsi ve svém povolání dobrý. Vzpomínáš na její varovný výkřik a na svou příliš pomalou reakci, když se na tebe sesunula hromada utržených balvanů.\nKdyž znovu procitneš, stojí proti tobě sám Popelavý král.\nUvědomíš si, že vlastně čelíš sám sobě.\nCelou tu dobu, od prvního procitnutí na zkrvaveném bojišti.\n“Skončeme to,” proneseš klidně.\nPopelavý král vyrazí bojový pokřik a hrne se vpřed… V jeho hlase ale nyní zaznívá strach.\nPokud je s tebou stále havran, otoč na 86\nPokud ne, hoď dvěma kostkami a sečti výsledná čísla.",
        "decisions": {
            "99": "Pokud je výsledek 7, nebo nižší, POpelavý král v posledním možném okamžiku zvítězí, když ti jeho čepel probodne hruď. Tvůj příběh zde na samé hranici vítězství končí",
            "85": "Pokud je výsledek 10 nebo více",
        },
    },
    {
        "id": 85,
        "paragraph": "Srazíte se s Popelavým králem znovu a znovu, až vaše meče křešou ve tmě ohnivé jiskry.\nPopelavý je hrozivým protivníkem… Ale nakonec stojí štěstěna při tobě.\nTvůj meč najde jeho prohnilé srdce a jednou provždy jej zahubí, ačkoliv stále má tvou vlastní podobu.\nJe zvláštní pozorovat sám sebe umírat…\nNáhle se cítíš strašlivě slabý, jakoby sis sám způsobil smrtelnou ránu.\nKlesáš na kolena, meč ti vypadne z ochablých prstů.\nTvůj poslední pohled na tomto světě patří vyjícím nemrtvým, kteří váš souboj sledovali u ústí strže.\nPak ale cosi protrhává temná mračna.\nSluneční paprsky pronikají šerou oponou mraků jako zlaté šípy a pod jejich dotekem se nemrtví mění v prach.\n“Vincero…” zašeptáš, i když význam toho slova ti uniká.\nNež tě opustí i poslední zbytky sil a ty propadneš mrákotám, v hlavě ti bleskne vzpomínka na tu podivnou operní árii.\nPak tě obestře temnota.\nA úsvit přichází.",
        "decisions": {"88": "Pokračuj"},
    },
    {
        "id": 86,
        "paragraph": "Hotovíš se k osudovému boji, znavený, ale odhodlaný.\nUděláš první krok vpřed, rozhodnut bojovat až do konce, trpaslík proti mocnému obru v černé zbroji.\nDémonický sok se jen zachechtá a vykročí ti vstříc.\nV tu chvíli slyšíš zakrákání.\nPak mezi vás dosedne malý havran.\nPopelavý král na okamžik zaváhá, pak ale pozvedne svou zbraň k útoku.\nA havran udeří hlavou o zem.\nV dalším okamžiku tě zaplaví vlna horka a ty jen užasle pozoruješ výjev před sebou.\nMísto vyčerpaného havrana před tebou poletuje plamenný fénix, nádherný ve své majestátnosti.\n“Tvá duše… Tak mocná! Nezlomená!”\nVíc toho Popelavý král nestihne říct.\nZ fénixova zobáku vyšlehne stěna plamene, která Popelavého krále během jediného úderu srdce pohltí.\nŽár je téměř nesnesitelný, až si musíš zastínit oči…\nKdyž je znovu otevřeš, vidíš, jak vedle nevelké hromádky popela poskakuje drobný černý havran s korálkovýma očima. Hlasitě zakráká, vzlétne… a pak ti znovu usedne na rameno.",
        "decisions": {"87": "Pokračuj"},
    },
    {
        "id": 87,
        "paragraph": "Náhle se cítíš strašlivě slabý, jakoby sis sám způsobil smrtelnou ránu.\nKlesáš na kolena, meč ti vypadne z ochablých prstů.\nTvůj poslední pohled na tomto světě patří vyjícím nemrtvým, kteří vše sledovali u ústí strže.\nPak ale cosi protrhává temná mračna.\nSluneční paprsky pronikají šerou oponou mraků jako zlaté šípy a pod jejich dotykem se nemrtví mění v prach.\n“Vincero…” zašeptáš, i když význam toho slova ti uniká.\nNež tě opustí i poslední zbytky sil a ty propadneš mrákotám, v hlavě ti bleskne vzpomínka na tu podivnou operní árii.\nPak tě obestře temnota.\nA úsvit přichází.",
        "decisions": {"88": "Pokračuj"},
    },
    {
        "id": 88,
        "paragraph": "Probudí tě bzučení a pípání přístrojů…\nA také nesnesitelná bolest.\nMáš tělo v jednom ohni a všimneš si, že jsi v podstatě celý obvázaný fáčem.\nOpatrně se rozhlédneš.\nLežíš na nemocničním lůžku jakési nemocnice, na nočním stolku vedle tebe stojí magnetofon, na kterém kdosi pustil nekonečnou smyčku jakési opery.\n“Al alba vincero!” zpívá právě jakýsi italský tenor.\nPomalu ti dochází, co se stalo.\nTa strž. Zle zraněný turista. Padající kameny, které jsi neviděl. A její varovný křik…\n“Panebože! Ty žiješ!”\nZ myšlenek tě vytrhne ženský výkřik plný dojetí.\nO chvíli později je u tebe mladá, pohledná blondýnka, která tě vší silou objímá, ačkoliv ti tak působí neskutečnou bolest… Chiara! Ano, tak se jmenuje.\n“Moc mě to mrzí. Nejdřív celou pomoc komplikovala noc… A pak… Když tě konečně operovali,” povídá s uslzenýma očima, “nedávali ti moc šancí. Říkali, že z komatu… se můžeš dostat jen sám. Nikdo jiný. Ale byla jsem tady. Mluvila jsem k tobě…  A ty ses mi vrátil.”\nTenor z nahrávky znovu zazpívá slavnou partii:\n“Al Alba vincero!”\nDívka si utře z tváře několik slz:\n“Vidíš, i tohle pomohlo. Al alba vincero - za úsvitu zvítězím. Hezký význam, ne? I Pucciniho opera ti pomohla se vrátit… Můj rytíři. Ty žiješ. Ty žiješ!”\nJak se ti střípky vzpomínek pomalu skládají v jasný obraz, uslyšíš jemné zaťukání na okno, kterým proniká sluneční svit.\nOpatrně otočíš hlavou… A zjistíš, že tě pozorují korálkové oči havrana.\nHladíš po vlasech Chiaru a děkuješ osudu, že tě zachoval při životě.\nBýt záchranářem je nebezpečné povolání… Tentokrát jsi ale vyvázl.\nHavran nad vším tím štěstím zavrtí hlavou, radostně zakráká…\nA v další chvíli je pryč.",
        "decisions": {"100": "KONEC"},
    },
)
