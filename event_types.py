from enum import Enum, auto


class EventType(Enum):
    DEATH = auto()
    DICE = auto()
    LIFE = auto()
    NAME = auto()
    PRINCESS = auto()
    UNKNOWN = auto()

    @classmethod
    def default(cls):
        return cls.UNKNOWN
