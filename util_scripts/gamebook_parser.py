import json
import re


def prepare_data_from_file(file_path: str) -> list[dict]:
    """
    parse a text file into dictionary that has first key as id (the paragraph number) and second key is the paragraph text
    :param file_path:
    :return: list of dictionaries that contain all gamebook data
    """
    # Open your file
    with open(
        file_path,
        "r",
        encoding="utf-8",
    ) as file:
        content = file.read()

    # Create a list to hold the dictionaries
    dict_list = []

    # Split the content by newline to get a list of lines
    lines = content.split("\n")

    # Temporary variables
    current_id = None
    current_paragraph = []

    # Process each line
    for line in lines:
        if line.isdigit():  # If the line is an id
            if current_id is not None:  # If a paragraph has been started, finish it
                dict_list.append(
                    {"id": current_id, "paragraph": " ".join(current_paragraph)}
                )
                current_paragraph = []
            current_id = int(line)  # Start a new paragraph
        else:  # If the line is part of a paragraph
            current_paragraph.append(f"{line.strip()}\n")

    # Add the last paragraph, if any
    if current_id is not None and current_paragraph:
        dict_list.append({"id": current_id, "paragraph": " ".join(current_paragraph)})

    return dict_list


def get_last_sentence_from_text(input_string: str):
    result = re.split(r"[.!?]+\s*", input_string)

    return result[-1]


# First we get all the paragraphs
gamebook_model = prepare_data_from_file(
    "C:\\Users\\Falco\\PycharmProjects\\gamebook-flask\\data\\prokleti_popelaveho_krale.txt"
)

# Then we check the paragraphs and extract the decisions to others
index = 4
for item in gamebook_model:
    result = re.split(r"[.!?]+\s*", item["paragraph"])[-1]
    result = re.split(r"(\d+)", result)
    try:
        result = [item for item in result if item.strip()]
        result = {result[i + 1]: result[i].strip() for i in range(0, len(result), 2)}
        item["decisions"] = result
    except:
        pass

    print(f"{item},")

with open("../data/prokleti_poplaveho_krale.json", "w", encoding="utf-8") as f:
    json.dump(gamebook_model, f)
