import logging
import re
from dataclasses import dataclass

from flask import Flask, render_template, request, session
from flask_bootstrap import Bootstrap, WebCDN
from flask_htmx import HTMX

import gamebook_data
from event_types import EventType

app = Flask(__name__)

app.secret_key = "CIumY8%$KAcIngMIjMqq!n8M80lTgrJX6okfhI%x"


Bootstrap(app)
htmx = HTMX(app)

logging.basicConfig(
    level=logging.DEBUG, format="%(asctime)s [%(levelname)s] - %(message)s"
)
app.extensions["bootstrap"]["cdns"]["jquery"] = WebCDN(
    "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/"
)
app.extensions["bootstrap"]["cdns"]["bootstrap"] = WebCDN(
    "https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.3/"
)


@dataclass
class PlayerCharacter:
    hp: int = 12
    name: str = "-"
    princess: str = "-"
    inventory: str = "-"
    notes: str = "-"
    current_paragraph: int = 0

    @classmethod
    def convert_str_to_dataclass(cls, dc_str):
        """
        :param dc_str: string that will be use to recreate a dataclass instance
        i.e. "PlayerCharacter(hp=12, name='Player name', princess='Chiara', inventory='-', notes='-')"
        :return: a dataclass instance
        :raises: ValueError in case that the string is not passed correctly
        """
        # Extract class name and init arguments from the string
        pattern = r"(\w+)\((.*)\)"
        match = re.match(pattern, dc_str)

        class_name = match.group(1)

        if cls.__name__ != class_name:
            raise ValueError(
                f"Class name {cls.__name__} does not match class name {class_name}."
            )

        args_str = match.group(2)

        # Convert the arguments to a dictionary
        args_dict = eval(f"dict({args_str})")

        # Get the class from globals and create an instance
        instance = PlayerCharacter(**args_dict)

        return instance


start_character: PlayerCharacter = PlayerCharacter()


@app.route("/")
def index():
    session["character"] = str(start_character)
    return render_template(
        "index.html",
        text=gamebook_data.popelavy_kral_data[0]["paragraph"],
        next_paragraph="1",
        decisions=gamebook_data.popelavy_kral_data[0]["decisions"],
        img=None,
    )


@app.route("/continue")
def continue_game():
    current_character = PlayerCharacter.convert_str_to_dataclass(session["character"])

    paragraph_data = gamebook_data.popelavy_kral_data[
        current_character.current_paragraph
    ]
    return render_template(
        "index.html",
        text=paragraph_data["paragraph"],
        next_paragraph=current_character.current_paragraph,
        decisions=paragraph_data["decisions"],
        img=None,
        update_stats=True,
    )


@app.route("/character")
def get_character():
    current_character = PlayerCharacter.convert_str_to_dataclass(session["character"])

    return render_template(
        "player_character.html",
        hp=current_character.hp,
        name=current_character.name,
        princess=current_character.princess,
        inventory=current_character.inventory,
        notes=current_character.notes,
    )


@app.route("/paragraph", methods=["POST"])
def submit():
    paragraph_id = int(request.form["paragraph-id"])

    next_paragraph_data: dict = gamebook_data.popelavy_kral_data[paragraph_id]

    img = None
    update_stats = False

    current_character = PlayerCharacter.convert_str_to_dataclass(session["character"])
    current_character.current_paragraph = paragraph_id

    if next_paragraph_data.get("events"):
        event_sub_dictionary: dict = next_paragraph_data["events"]
        for event, value in event_sub_dictionary.items():
            if EventType[event.upper()] is EventType.DEATH:
                img = "image/Popelavy_kral.png"
            elif EventType[event.upper()] is EventType.LIFE:
                update_stats = True
                current_character.hp += int(value)
                if current_character.hp <= 0:
                    img = "image/Popelavy_kral.png"
            elif EventType[event.upper()] is EventType.NAME:
                update_stats = True
                current_character.name = value
            elif EventType[event.upper()] is EventType.PRINCESS:
                update_stats = True
                current_character.princess = value

    session["character"] = str(current_character)

    return render_template(
        "gamebook_paragraph.html",
        text=next_paragraph_data["paragraph"],
        next_paragraph=paragraph_id + 1,
        decisions=next_paragraph_data["decisions"],
        img=img,
        update_stats=update_stats,
    )


if __name__ == "__main__":
    app.run()
